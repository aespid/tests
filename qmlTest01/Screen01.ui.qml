import QtQuick 2.12
import qmlTest01 1.0
import QtQuick.Timeline 1.0
import QtQuick.Controls 2.3
import QtQuick.Studio.Components 1.0

Rectangle {
    width: Constants.width
    height: Constants.height

    color: Constants.backgroundColor
    antialiasing: false

    Text {
        text: qsTr("Hello qmlTest01")
        anchors.centerIn: parent
        font.family: Constants.font.family
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                running: true
                from: 0
                loops: 1
                duration: 1000
                to: 1000
            }
        ]
        enabled: true
        endFrame: 1000
        startFrame: 0

        KeyframeGroup {
            target: arc
            property: "end"
            Keyframe {
                value: 0
                frame: 0
            }

            Keyframe {
                value: 180
                frame: 1000
            }
        }
    }

    ArcItem {
        id: arc
        x: 170
        y: 316
        width: 668
        height: 100
        antialiasing: true
        capStyle: 32
        strokeWidth: 5
        end: 180
        arcWidth: 18
        strokeColor: "#547c16"
        fillColor: "#00000000"
    }

    Image {
        id: image
        x: 399
        y: 478
        source: "qml_tree.png"
        fillMode: Image.PreserveAspectFit
    }
}




/*##^## Designer {
    D{i:2}
}
 ##^##*/
