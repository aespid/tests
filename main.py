import os
import sys

from PySide2.QtCore import QUrl
from PySide2.QtGui import QGuiApplication
from PySide2.QtQuick import *

if __name__ == "__main__":
    current_dir = os.path.dirname(os.path.realpath(__file__))

    os.environ["QT_QUICK_CONTROLS_CONF"] = os.path.join(current_dir, "qmlTest01", "qtquickcontrols2.conf")
    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    os.environ["QML_IMPORT_TRACE "] = "1"

    app = QGuiApplication(sys.argv)
    view = QQuickView()
    import_path = os.path.join(current_dir, "qmlTest01", "imports")
    view.engine().addImportPath(import_path)
    print(view.engine().importPathList())

    filename = os.path.join(current_dir, "qmlTest01", "qmlTest01.qml")
    url = QUrl.fromLocalFile(filename)

    view.setSource(url)
    view.show()
    app.exec_()